def get_code(moves, start):
    pos = start
    for move in moves:
        if move == 'U' and pos > 3:
            pos -= 3
        elif move == 'D' and pos < 7:
            pos += 3
        elif move == 'L' and pos not in (1, 4, 7):
            pos -= 1
        elif move == 'R' and pos not in (3, 6, 9):
            pos += 1

    return pos

with open('02_input.txt') as f:
    num = 5
    code = ''
    for line in f:
        num = get_code(line, num)
        code += str(num)
    print(code)
