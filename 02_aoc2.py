keypad_input = '''--1--
-234-
56789
-ABC-
--D--'''

keypad = keypad_input.split()
#print(keypad)

def is_valid_position(row, col):
    return 0 <= row <= 4 and 0 <= col <= 4 and keypad[row][col] != '-'

def get_new_position(moves, row, col):
    for move in moves:
        if move == 'U' and is_valid_position(row - 1, col):
            row -= 1
        elif move == 'D' and is_valid_position(row + 1, col):
            row += 1
        elif move == 'L' and is_valid_position(row, col - 1):
            col -= 1
        elif move == 'R' and is_valid_position(row, col + 1):
            col += 1

        #print(move, row, col, keypad[row][col])

    return row, col

with open('02_input.txt') as f:
    row, col = 3, 0
    code = ''
    for line in f:
        row, col = get_new_position(line, row, col)
        code += keypad[row][col]
    print(code)
