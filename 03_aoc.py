with open('03_input.txt') as f:
    possible = 0
    for line in f:
        a, b, c = [int(s) for s in line.split()]
        if a + b > c and a + c > b and b + c > a:
            possible += 1

    print(possible)
