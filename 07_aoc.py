import re

def has_abba(s):
    if len(s) < 4:
        return False

    for i in range(len(s) - 3):
        if s[i] != s[i+1] and s[i:i+2] == s[i+3:i+1:-1]:
            #print(s, i, s[i:i+4])
            return True

    return False

support_tls = 0

with open('07_input.txt') as f:
    for line in f:
        ip_parts = re.split('\[|\]', line.strip())
        if (any(has_abba(p) for p in ip_parts[::2])
                and not any(has_abba(p) for p in ip_parts[1::2])):
            support_tls += 1

print(support_tls)
