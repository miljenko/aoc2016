import heapq
from itertools import combinations

def floor_valid(floor):
    if not any(i > 0 for i in floor):
        return True

    return all(-chip in floor for chip in floor if chip < 0)

def heuristic(floors):
    return -(len(floors[-1]) * 5 + len(floors[-2]) * 2)

start = (
    0, # elevator floor
    (
        # floors (> 0 - generators, < 0 - chips)
        frozenset([1, -1]),
        frozenset([2, 3, 4, 5]),
        frozenset([-2, -3, -4, -5]),
        frozenset()
    )
)

frontier = []
heapq.heappush(frontier, (0, start))
cost_so_far = {start: 0}
# states = 0

while frontier:
    priority, current = heapq.heappop(frontier)
    elevator_floor, floors = current

    if all(len(f) == 0 for f in floors[:-1]):
        break

    directions = [d for d in (-1, 1) if 0 <= elevator_floor + d <= 3]
    cargos = list(combinations(floors[elevator_floor], 2)) + [(i,) for i in floors[elevator_floor]]

    for cargo in cargos:
        for direction in directions:
            next_floors = list(floors)
            next_floors[elevator_floor] -= set(cargo)
            next_floors[elevator_floor + direction] |= set(cargo)

            if (not floor_valid(next_floors[elevator_floor])
                    or not floor_valid(next_floors[elevator_floor + direction])):
                continue

            # states += 1
            # if states % 1000000 == 0:
            #     print(states)

            next_state = elevator_floor + direction, tuple(next_floors)
            new_cost = cost_so_far[current] + 1
            if next_state not in cost_so_far or new_cost < cost_so_far[next_state]:
                cost_so_far[next_state] = new_cost
                priority = new_cost + heuristic(next_floors)
                heapq.heappush(frontier, (priority, next_state))

# print(states)
print(cost_so_far[current])
