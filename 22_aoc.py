import re

def viable(a, b):
    return a[0] > 0 and a[0] <= b[1]

df_re = re.compile('/dev/grid/node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)%')

ROWS, COLS = 30, 34
grid = [[0 for c in range(COLS)] for r in range(ROWS)]

with open('22_input.txt') as f:
    for i, line in enumerate(f):
        m = df_re.match(line)
        if m:
            #print('{} {}'.format(i, m.groups()))
            x, y, size, used, avail, use = (int(n) for n in m.groups())
            grid[y][x] = (used, avail)

counter = 0

for r in range(ROWS):
    for c in range(COLS):
        for rr in range(ROWS):
            for cc in range(COLS):
                if (r != rr or c != cc) and viable(grid[r][c], grid[rr][cc]):
                    counter += 1

print(counter)
