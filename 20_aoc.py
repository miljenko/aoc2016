ranges = []
with open('20_input.txt') as f:
    for line in f:
        ranges.append([int(i) for i in line.strip().split('-')])

ranges.sort()
last_high = 0

for r in ranges:
    if r[0] > last_high + 1:
        break
    if r[1] > last_high:
        last_high = r[1]

print(last_high + 1)
