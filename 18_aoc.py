def next_row(row):
    row = '.' + row + '.'
    new_row = ''
    for i in range(1, len(row) - 1):
        new_row += '^' if row[i-1] != row[i+1] else '.'

    return new_row

with open('18_input.txt') as f:
    row = f.read().strip()

counter = 0
for _ in range(40):
    counter += row.count('.')
    row = next_row(row)

print(counter)
