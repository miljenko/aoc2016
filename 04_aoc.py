import re
from collections import Counter

sum_sectors = 0

with open('04_input.txt') as f:
    for line in f:
        parts = re.split('\W+', line)
        del parts[-1]
        #print(parts)
        room = ''.join(parts[:-2])
        sector = int(parts[-2])
        checksum = parts[-1]
        #print(room, sector, checksum)
        cnt = Counter(room)
        srt = sorted(cnt.items(), key=lambda x: (-x[1], x[0]))
        top5 = ''.join(s[0] for s in srt[:5])
        #print(top5)

        if checksum == top5:
            sum_sectors += sector

print(sum_sectors)
