import re

marker_re = re.compile('\((\d+)x(\d+)\)')

def get_decompressed_length(s):
    decompressed_length = 0
    i = 0

    while i < len(s):
        if s[i] == '(':
            marker = s[i: s.index(')', i) + 1]
            data_length, n = (int(g) for g in marker_re.search(marker).groups())
            data = s[i + len(marker): i + len(marker) + data_length]

            decompressed_length += (get_decompressed_length(data)
                                    if '(' in data else data_length) * n

            i += len(marker) + data_length
            continue
        else:
            decompressed_length += 1
            i += 1

    return decompressed_length

with open('09_input.txt') as f:
    compressed = f.read().strip()

print(get_decompressed_length(compressed))
