import hashlib

door = 'ffykfhsq'
i = 0
password = ''

while len(password) < 8:
    hash_ = hashlib.md5((door + str(i)).encode()).hexdigest()
    if hash_.startswith('00000'):
        password += hash_[5]
        print(len(password), i, password)
    i += 1

print(password)
