with open('01_input.txt') as f:
    inp = f.read()

instructions = [(el[0], int(el[1:])) for el in (el.strip() for el in inp.split(','))]
#print(instructions)

d = 'N'
x, y = 0, 0
visited = {(0, 0): 1}
first_twice = True

directions = {
    'N': (0, 1),
    'E': (1, 0),
    'S': (0, -1),
    'W': (-1, 0),
}

turns = {
    'N': {'R': 'E', 'L': 'W'},
    'E': {'R': 'S', 'L': 'N'},
    'S': {'R': 'W', 'L': 'E'},
    'W': {'R': 'N', 'L': 'S'},
}

for i in instructions:
    d = turns[d][i[0]]
    kx, ky = directions[d]

    newx = x + i[1] * kx
    newy = y + i[1] * ky

    if kx == 0:
        cx = x
        rng = range(y + ky, newy + ky, ky)
    else:
        cy = y
        rng = range(x + kx, newx + kx, kx)

    for c in rng:
        if kx == 0:
            cy = c
        else:
            cx = c

        #print(cx, cy)
        if (cx, cy) in visited:
            if first_twice:
                print('First visited twice: {}, {} (distance: {})'.format(cx, cy, abs(cx) + abs(cy)))
                first_twice = False
        else:
            visited[(cx, cy)] = 1

    x = newx
    y = newy

    #print(i, d, x, y)

print('Final position: {}, {} (distance: {})'.format(x, y, abs(x) + abs(y)))
