def get_param_value(params, i, registers):
    return params[i] if isinstance(params[i], int) else registers[params[i]]

def run_program(a):
    last_out = 1
    out_count = 0
    registers = {'a': a, 'b': 0, 'c': 0, 'd': 0}
    lineno = 0
    while lineno < len(program):
        instr, params = program[lineno]
        #print(instr, params)

        if instr == 'cpy':
            registers[params[1]] = get_param_value(params, 0, registers)
        elif instr == 'inc':
            registers[params[0]] += 1
        elif instr == 'dec':
            registers[params[0]] -= 1
        elif instr == 'jnz':
            if get_param_value(params, 0, registers) != 0:
                lineno += get_param_value(params, 1, registers)
                continue
        elif instr == 'out':
            out = get_param_value(params, 0, registers)
            if out == last_out or out > 1 or out < 0:
                return False
            out_count += 1
            if out_count > 100:
                return True
            last_out = out

        lineno += 1

    return True

program = []
with open('25_input.txt') as f:
    for line in f:
        instr, params = line[:3], [p if p in 'abcd' else int(p)
                                   for p in line[3:].strip().split()]
        program.append((instr, params))

i = 0
while not run_program(i):
    i += 1

print(i)
