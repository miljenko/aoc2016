import hashlib

door = 'ffykfhsq'
i = 0
password = ['_'] * 8

while any(c == '_' for c in password):
    hash_ = hashlib.md5((door + str(i)).encode()).hexdigest()
    if hash_.startswith('00000') and '0' <= hash_[5] <= '7':
        pos = int(hash_[5])
        if password[pos] == '_':
            password[pos] = hash_[6]
            print(pos, i, ''.join(password))
    i += 1

print(''.join(password))
