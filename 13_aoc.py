from collections import deque

start = 1, 1
goal = 31, 39

def is_open(x, y):
    v = x*x + 3*x + 2*x*y + y + y*y + 1350
    n = bin(v).count('1')
    return n % 2 == 0

def neighbors(x, y):
    yield x+1, y
    yield x, y+1
    if x > 0:
        yield x-1, y
    if y > 0:
        yield x, y-1

frontier = deque([start])
came_from = {start: start}
cost_so_far = {start: 0}

while frontier:
    current = frontier.popleft()

    if current == goal:
        break

    for neighbor in neighbors(*current):
        if neighbor not in came_from and is_open(*neighbor):
            frontier.append(neighbor)
            came_from[neighbor] = current
            cost_so_far[neighbor] = cost_so_far[current] + 1

print(cost_so_far[goal])
