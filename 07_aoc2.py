import re

def get_abas(strings):
    for s in strings:
        if len(s) < 3:
            continue

        for i in range(len(s) - 2):
            if s[i] != s[i+1] and s[i] == s[i+2]:
                #print(s, i, s[i:i+3])
                yield s[i:i+3]

def invert_aba(aba):
    return aba[1:] + aba[1]

support_ssl = 0

with open('07_input.txt') as f:
    for line in f:
        ip_parts = re.split('\[|\]', line.strip())
        for aba in get_abas(ip_parts[::2]):
            bab = invert_aba(aba)
            if any(bab in p for p in ip_parts[1::2]):
                support_ssl += 1
                break

print(support_ssl)
