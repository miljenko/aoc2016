class Node():
    def __init__(self, index):
        self.count = 1
        self.index = index
        self.previous = None
        self.next = None

    def add(self, node):
        self.next = node
        node.previous = self

    def drop(self):
        self.previous.next = self.next
        self.next.previous = self.previous

    def __str__(self):
        return '{} <- {} ({}) -> {}'.format(
            self.previous.index, self.index, self.count, self.next.index)

n = 3018458

head = Node(1)
node = head
for i in range(2, n+1):
    nn = Node(i)
    node.add(nn)
    if i == n // 2 + 1:
        steal_from = nn
    node = nn
node.next = head
head.previous = node

node = head
while node.next != node:
    node.count += steal_from.count
    next_steal_from = steal_from.next if n % 2 == 0 else steal_from.next.next
    steal_from.drop()
    steal_from = next_steal_from
    n -= 1
    node = node.next

print(node.index)
