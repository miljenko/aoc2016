from itertools import zip_longest

def grouper(iterable, n):
    args = [iter(iterable)] * n
    return zip_longest(*args)

def generate(a):
    while len(a) < disk_size:
        b = ''.join('1' if c == '0' else '0' for c in a[::-1])
        a = a + '0' + b
    return a[:disk_size]

def calc_checksum(data):
    return ''.join('1' if g[0] == g[1] else '0' for g in grouper(data, 2))

input_data = '11110010111001001'
disk_size = 272

disk_data = generate(input_data)
checksum = calc_checksum(disk_data)
while len(checksum) % 2 == 0:
    checksum = calc_checksum(checksum)

print(checksum)
