from collections import Counter

with open('06_input.txt') as f:
    lines = f.readlines()

message = ''
for i in range(len(lines[0]) - 1):
    message += Counter(line[i] for line in lines).most_common()[-1][0]
    #print(i, message)

print(message)
