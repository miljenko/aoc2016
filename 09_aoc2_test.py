import importlib
import unittest

m = importlib.import_module('09_aoc2')

class DecompressTest(unittest.TestCase):
    def test(self):
        self.assertEqual(m.get_decompressed_length('(3x3)XYZ'), 9)
        self.assertEqual(m.get_decompressed_length('X(8x2)(3x3)ABCY'), 20)
        self.assertEqual(m.get_decompressed_length('(27x12)(20x12)(13x14)(7x10)(1x12)A'), 241920)
        self.assertEqual(m.get_decompressed_length(
            '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN'), 445)

unittest.main()
