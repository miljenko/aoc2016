import re
from string import ascii_lowercase

with open('04_input.txt') as f:
    for line in f:
        parts = re.split('\W+', line)
        del parts[-1]
        #print(parts)
        room = ''.join(parts[:-2])
        sector = int(parts[-2])
        decrypted = ''
        for c in room:
            enc_idx = ascii_lowercase.index(c)
            dec_idx = (enc_idx + sector) % 26
            decrypted += ascii_lowercase[dec_idx]

        #print('{} -> {}'.format(room, decrypted))
        if 'north' in decrypted:
            print(decrypted)
            print(sector)
