with open('01_input.txt') as f:
    inp = f.read()

instructions = [(el[0], int(el[1:])) for el in (el.strip() for el in inp.split(','))]
#print(instructions)

d = 'N'
x, y = 0, 0

directions = {
    'N': (0, 1),
    'E': (1, 0),
    'S': (0, -1),
    'W': (-1, 0),
}

turns = {
    'N': {'R': 'E', 'L': 'W'},
    'E': {'R': 'S', 'L': 'N'},
    'S': {'R': 'W', 'L': 'E'},
    'W': {'R': 'N', 'L': 'S'},
}

for i in instructions:
    d = turns[d][i[0]]
    x += i[1] * directions[d][0]
    y += i[1] * directions[d][1]

    #print(i, d, x, y)

print('Final position: {}, {} (distance: {})'.format(x, y, abs(x) + abs(y)))
