import re

discs = []
input_re = re.compile('\d+')

with open('15_input.txt') as f:
    for line in f:
        _, period, _, start = (int(n) for n in input_re.findall(line))
        discs.append((period, start))

t = 0
while any((t + i + disc[1]) % disc[0] != 0 for i, disc in enumerate(discs, start=1)):
    t += 1

print(t)
