from collections import deque
from itertools import combinations

def is_open(r, c):
    return grid[r][c] != '#'

def neighbors(r, c):
    yield r+1, c
    yield r, c+1
    if r > 0:
        yield r-1, c
    if c > 0:
        yield r, c-1

def shortest_path(start, goal):
    frontier = deque([start])
    came_from = {start: start}
    cost_so_far = {start: 0}

    while frontier:
        current = frontier.popleft()

        if current == goal:
            break

        for neighbor in neighbors(*current):
            if neighbor not in came_from and is_open(*neighbor):
                frontier.append(neighbor)
                came_from[neighbor] = current
                cost_so_far[neighbor] = cost_so_far[current] + 1

    return cost_so_far[goal]

with open('24_input.txt') as f:
    grid = [line.strip() for line in f]

goals = {}
for r, row in enumerate(grid):
    for c, s in enumerate(row):
        if s in '0123456789':
            goals[s] = (r, c)

distances = [[0 for _ in range(8)] for _ in range(8)]
for s in range(8):
    for g in range(s+1, 8):
        distances[g][s] = distances[s][g] = shortest_path(goals[str(s)], goals[str(g)])

# for row in distances:
#     print(row)

# Held-Karp algorithm
tsp_cost = {}
for i in range(8):
    for j in range(i+1, 8):
        tsp_cost[frozenset((i, j)), i] = tsp_cost[frozenset((i, j)), j] = distances[i][j]

for subset_len in range(3, 9):
    for subset in combinations(range(8), subset_len):
        sub = frozenset(subset)
        for k in subset:
            tsp_cost[sub, k] = min(tsp_cost[sub - {k}, m] + distances[m][k] for m in sub - {0, k})

min_distance = min(tsp_cost[frozenset(range(1, 8)), k] + distances[0][k] for k in range(1, 8))
print(min_distance)
