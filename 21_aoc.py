import re

def get_min_max(pos1, pos2):
    pos1, pos2 = int(pos1), int(pos2)
    return (pos1, pos2) if pos1 < pos2 else (pos2, pos1)

def swap_positions(s, pos1, pos2):
    first, second = get_min_max(pos1, pos2)
    return s[:first] + s[second] + s[first+1:second] + s[first] + s[second+1:]

def swap_letters(s, letter1, letter2):
    pos1, pos2 = s.index(letter1), s.index(letter2)
    return swap_positions(s, pos1, pos2)

def rotate(s, direction, steps):
    steps = int(steps)
    if direction == 'left':
        steps = -steps

    return s[-steps:] + s[:-steps]

def rotate_by_letter(s, letter):
    idx = s.index(letter)
    steps = idx + 2 if idx >= 4 else idx + 1
    return rotate(s, 'right', steps % len(s))

def reverse(s, pos1, pos2):
    pos1, pos2 = int(pos1), int(pos2)
    return s[:pos1] + s[pos1:pos2+1][::-1] + s[pos2+1:]

def move(s, pos1, pos2):
    pos1, pos2 = int(pos1), int(pos2)
    l = s[pos1]
    s = s[:pos1] + s[pos1+1:]
    return s[:pos2] + l + s[pos2:]

password = 'abcdefgh'
instructions = [
    (re.compile('swap position (\d+) with position (\d+)'), swap_positions),
    (re.compile('swap letter (\w) with letter (\w)'), swap_letters),
    (re.compile('rotate (left|right) (\d+) step'), rotate),
    (re.compile('rotate based on position of letter (\w)'), rotate_by_letter),
    (re.compile('reverse positions (\d+) through (\d+)'), reverse),
    (re.compile('move position (\d+) to position (\d+)'), move)
]

with open('21_input.txt') as f:
    for i, line in enumerate(f):
        for instr_re, func in instructions:
            m = instr_re.match(line)
            if m:
                password = func(password, *m.groups())
                print('{} {}{} -> {}'.format(i, func.__name__, m.groups(), password))
                break

print(password)
