def triangle_possible(a, b, c):
    return a + b > c and a + c > b and b + c > a

with open('03_input.txt') as f:
    possible = 0
    lines = []
    for line in f:
        lines.append(line.split())
        if len(lines) == 3:
            for i in range(3):
                a, b, c = [int(line[i]) for line in lines]
                if triangle_possible(a, b, c):
                    possible += 1
            lines = []

    print(possible)
