import re

df_re = re.compile('/dev/grid/node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)%')

ROWS, COLS = 30, 34
grid = [[0 for c in range(COLS)] for r in range(ROWS)]

with open('22_input.txt') as f:
    for i, line in enumerate(f):
        m = df_re.match(line)
        if m:
            #print('{} {}'.format(i, m.groups()))
            x, y, size, used, avail, use = (int(n) for n in m.groups())
            grid[y][x] = (used, avail, use)

for row in grid:
    print(''.join('_' if x[2] == 0 else '#' if x[2] > 85 else '.' for x in row))

print(25+3+32+(32*5)) # (up + left (around wall) + right + left (5 steps to go around goal))
