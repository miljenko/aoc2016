from bisect import insort
from hashlib import md5

def first_3_in_a_row(s):
    if len(s) < 3:
        return None

    for n in range(len(s) - 2):
        if s[n] == s[n+1] == s[n+2]:
            return s[n]

    return None

salt = 'cuanljph'
possible_keys = []
key_indexes = []
i = 0

while len(key_indexes) < 64 or (possible_keys and possible_keys[0][0] < key_indexes[63]):
    hash_ = md5((salt + str(i)).encode()).hexdigest()
    repeated = first_3_in_a_row(hash_)

    if repeated is not None:
        keys_to_remove = []
        for pk in possible_keys:
            if pk[0] < i - 1000:
                keys_to_remove.append(pk)
                continue
            if hash_.find(pk[1]*5) > -1:
                insort(key_indexes, pk[0])
                #print(pk, i, key_indexes[min(len(key_indexes) - 1, 63)])
                keys_to_remove.append(pk)

        for k in keys_to_remove:
            possible_keys.remove(k)

        possible_keys.append((i, repeated))

    i += 1

#print(key_indexes)
#print(len(key_indexes))
print(key_indexes[63])
