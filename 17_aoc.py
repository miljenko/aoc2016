from collections import deque
from hashlib import md5

def neighbors(r, c):
    if r < 3:
        yield (r+1, c), 'D'
    if c < 3:
        yield (r, c+1), 'R'
    if r > 0:
        yield (r-1, c), 'U'
    if c > 0:
        yield (r, c-1), 'L'

def get_doors(path):
    hash_ = md5((passcode + path).encode()).hexdigest()
    keys = 'UDLR'
    values = [c in 'bcdef' for c in hash_[:4]]
    return dict(zip(keys, values))

def shortest_path():
    frontier = deque([(start, '')])

    while frontier:
        current, path = frontier.popleft()
        door_open = get_doors(path)

        for next_position, move in neighbors(*current):
            if not door_open[move]:
                continue

            if next_position == goal:
                return path + move
            else:
                frontier.append((next_position, path + move))

passcode = 'mmsxrhfx'
start = (0, 0)
goal = (3, 3)

print(shortest_path())
