import re

def decompress(s):
    marker_re = re.compile('(\d+)x(\d+)')
    decompressed = ''
    i = 0

    while i < len(s):
        if s[i] == '(':
            marker = s[i: s.index(')', i) + 1]
            data_length, n = (int(g) for g in marker_re.search(marker).groups())
            data = s[i + len(marker): i + len(marker) + data_length]
            decompressed += data * n
            i += len(marker) + data_length
            continue
        else:
            decompressed += compressed[i]
            i += 1

    return decompressed


with open('09_input.txt') as f:
    compressed = f.read().strip()

decompressed = decompress(compressed)
print(len(decompressed))
