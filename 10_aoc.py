import re
from collections import namedtuple

get_re = re.compile('value (\d+) goes to bot (\d+)')
give_re = re.compile('bot (\d+) gives low to ([a-z]+) (\d+) and high to ([a-z]+) (\d+)')

Instruction = namedtuple('Instruction', ['bot', 'recv_low', 'idx_low', 'recv_high', 'idx_high'])
bots = [[] for i in range(210)]
queue = []

with open('10_input.txt') as f:
    for line in f:
        m = get_re.match(line)
        if m:
            val, bot = (int(g) for g in m.groups())
            bots[bot].append(val)
        else:
            m = give_re.match(line)
            bot, recv_low, idx_low, recv_high, idx_high = m.groups()
            queue.append(Instruction(int(bot), recv_low, int(idx_low), recv_high, int(idx_high)))

i = 0
while queue:
    if i == len(queue):
        i = 0
    ins = queue[i]
    chips = bots[ins.bot]
    if len(chips) < 2:
        i += 1
    else:
        if 17 in chips and 61 in chips:
            print(ins.bot)
            break
        if ins.recv_low == 'bot':
            bots[ins.idx_low].append(min(chips))
        if recv_high == 'bot':
            bots[ins.idx_high].append(max(chips))
        bots[ins.bot] = []
        del queue[i]
