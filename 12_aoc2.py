program = []
registers = {'a': 0, 'b': 0, 'c': 1, 'd': 0}

with open('12_input.txt') as f:
    for line in f:
        instr, params = line[:3], [p if p in registers else int(p)
                                   for p in line[3:].strip().split()]
        program.append((instr, params))

lineno = 0
while lineno < len(program):
    instr, params = program[lineno]
    #print(instr, params)

    if instr == 'cpy':
        registers[params[1]] = params[0] if isinstance(params[0], int) else registers[params[0]]
    elif instr == 'inc':
        registers[params[0]] += 1
    elif instr == 'dec':
        registers[params[0]] -= 1
    elif instr == 'jnz':
        if (params[0] if isinstance(params[0], int) else registers[params[0]]) != 0:
            lineno += params[1]
            continue

    lineno += 1

#print(registers)
print(registers['a'])
