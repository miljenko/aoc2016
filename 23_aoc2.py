def get_param_value(params, i):
    return params[i] if isinstance(params[i], int) else registers[params[i]]

program = []
registers = {'a': 12, 'b': 0, 'c': 0, 'd': 0}

with open('23_input.txt') as f:
    for line in f:
        instr, params = line[:3], [p if p in registers else int(p)
                                   for p in line[3:].strip().split()]
        program.append((instr, params))

lineno = 0
while lineno < len(program):
    instr, params = program[lineno]
    #print(registers)
    #print(lineno, instr, params)

    if lineno == 4:
        registers['a'] += registers['b'] * registers['d']
        registers['c'] = 0
        registers['d'] = 0
        lineno += 6
        continue

    if instr == 'cpy':
        registers[params[1]] = get_param_value(params, 0)
    elif instr == 'inc':
        registers[params[0]] += 1
    elif instr == 'dec':
        registers[params[0]] -= 1
    elif instr == 'jnz':
        if get_param_value(params, 0) != 0:
            lineno += get_param_value(params, 1)
            continue
    elif instr == 'tgl':
        offset = get_param_value(params, 0)
        tgl_lineno = lineno + offset
        if tgl_lineno >= len(program):
            lineno += 1
            continue
        instr_tgl, instr_tgl_params = program[tgl_lineno]
        if instr_tgl == 'inc':
            instr_tgl = 'dec'
        elif len(instr_tgl_params) == 1:
            instr_tgl = 'inc'
        if instr_tgl == 'jnz':
            instr_tgl = 'cpy'
        elif len(instr_tgl_params) == 2:
            instr_tgl = 'jnz'

        program[tgl_lineno] = (instr_tgl, instr_tgl_params)

    lineno += 1

#print(registers)
print(registers['a'])
