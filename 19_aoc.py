def one_has_all(l, n):
    for i in l:
        if i == n:
            return True
        elif i > 0:
            return False

n = 3018458
l = [1] * n

while not one_has_all(l, n):
    i = 0
    while i < n:
        if l[i] == 0:
            i += 1
            continue
        j = i+1 if i+1 < n else 0
        while l[j] == 0 and j != i:
            j = j+1 if j+1 < n else 0
        l[i] += l[j]
        l[j] = 0
        i += 1

print(l.index(n) + 1)
