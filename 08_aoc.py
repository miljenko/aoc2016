import re

grid = [[0 for _ in range(50)] for _ in range(6)]
params_re = re.compile('(\d+)[^\d]+(\d+)')

def rotr(r, n):
    grid[r] = grid[r][-n:] + grid[r][:-n]

def rotc(c, n):
    oldc = [grid[r][c] for r in range(6)]
    newc = oldc[-n:] + oldc[:-n]
    for i, val in enumerate(newc):
        grid[i][c] = val

def rect(x, y):
    for r in range(y):
        for c in range(x):
            grid[r][c] = 1

with open('08_input.txt') as f:
    for line in f:
        if line.startswith('rotate row'):
            op = rotr
        elif line.startswith('rotate column'):
            op = rotc
        elif line.startswith('rect'):
            op = rect

        p1, p2 = (int(p) for p in params_re.search(line).groups())
        #print('{}({}, {})'.format(op.__name__, p1, p2))
        op(p1, p2)

print(sum(sum(row) for row in grid))

for row in range(6):
    print(''.join('#' if c == 1 else ' ' for c in grid[row]))

# AFBUPZBJPS
